package TAX;

import java.util.ArrayList;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	ArrayList<Taxable> p = new ArrayList<Taxable>();
	p.add(new Person("pokko",100000));
	p.add(new Person("kai",200000));

	ArrayList<Taxable> c = new ArrayList<Taxable>();
	c.add(new Company("AIS",300000,100000));
	c.add(new Company("TRUE",250000,120000));
	
	ArrayList<Taxable> pr = new ArrayList<Taxable>();
	pr.add(new Product("iphone",20000));
	pr.add(new Product("ipad",30000));
	
	Calculator cal =new Calculator();
	System.out.println("your total person tax "+cal.total(p));
	System.out.println("your total company tax "+cal.total(c));
	System.out.println("your total product tax "+cal.total(pr));
	
	ArrayList<Taxable> sum = new ArrayList<Taxable>();
	sum.add(new Person("pokko",100000));
	sum.add(new Company("AIS",300000,100000));
	sum.add(new Product("iphone",20000));
	System.out.println("your total tax is "+cal.total(sum));
	}	
	
}
