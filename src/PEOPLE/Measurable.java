package PEOPLE;
public interface Measurable
 {
 	/**
 	 * Computes the measue of the object.
 	 * @return the measure
 	 */
 	 double getMeasure();
 }