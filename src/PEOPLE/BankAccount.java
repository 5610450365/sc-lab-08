package PEOPLE;

public class BankAccount implements Measurable{
	String name;
	double balance;
	public BankAccount(String intialname,double intialBalance){
		name = intialname;
		balance = intialBalance;
	}
	public void deposit(double amount){
		balance = balance+amount;
	}
	public void withdraw(double amount){
		if(balance < amount){
			balance = balance-amount;
		}
	}
	public double getBalance(){
		return balance;
	}
	public double getMeasure(){
		return balance;
		
	}
	public String getName(){
		return name;
	}
	

}
